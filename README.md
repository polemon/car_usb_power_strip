# car_usb_power_strip

Kicad PCB project for a simple USB car power strip

***

![Power Strip Schematic](./car_usb_power_strip.png)*Car Power Strip*

## Description
Simple USB car power strip for the auxiliaries 12V power outlet


## License
OSH license applies.
